import { JsomNode } from 'sp-jsom-node';
import { authConfig } from '../config';

(new JsomNode(authConfig)).wizard().then((settings) => {
    var web, clientContext, currentUser, oList;
    clientContext = SP.ClientContext.get_current();
    web = clientContext.get_web();
    var customList = clientContext.get_web().get_lists().getByTitle('Lista z JSOMa2');
    var listItem = customList.getByTitle('test1');
    listItem.deleteObject();
    clientContext.executeQueryAsync(
        function () { console.log('usunąłem: ' + 'test1'); },
        function (sender, args) { console.log('Error: ' + args.get_message()); }
    );
}).catch(console.log);