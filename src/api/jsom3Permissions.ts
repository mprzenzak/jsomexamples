import { JsomNode } from 'sp-jsom-node';
import { authConfig } from '../config';

(new JsomNode(authConfig)).wizard().then((settings) => {
  var web, clientContext, currentUser, oList;
  clientContext = SP.ClientContext.get_current();
  web = clientContext.get_web();
  currentUser = web.get_currentUser();
  oList = web.get_lists().getByTitle('Lista do uprawnien');
  //EffectiveBasePermissions - uprawnienia jakie ma uzytkownik
  clientContext.load(oList, 'EffectiveBasePermissions');
  clientContext.load(currentUser);
  clientContext.load(web);
  clientContext.executeQueryAsync(function () {
    if (oList.get_effectiveBasePermissions().has(SP.PermissionKind.editListItems)) {
      console.log("sa uprawnienia");
    } else {
      console.log("nie ma uprawnien");
    }
  }, function (sender, args) {
    console.log('blad');
  });

}).catch(console.log);