import { JsomNode } from 'sp-jsom-node';
import { authConfig } from './../config';

(new JsomNode(authConfig)).wizard().then((settings) => {

  let ctx = SP.ClientContext.get_current();
  let web = ctx.get_web();
  ctx.load(web);
  var listCreationInfo = new SP.ListCreationInformation();
  listCreationInfo.set_title('Lista z JSOMa6');
  listCreationInfo.set_templateType(SP.ListTemplateType.genericList);
  var myList = web.get_lists().add(listCreationInfo);
  ctx.load(myList);

  ctx.executeQueryAsync(() => {
    console.log(`Nazwa sita: ${web.get_title()}`),
    console.log(`Lista pobrana z sita: ${web.getList('https://dikf365.sharepoint.com/sites/dev/Lists/Lista%20do%20uprawnien/AllItems.aspx')}`);

    var lista = web.getList('https://dikf365.sharepoint.com/sites/dev/Lists/Lista%20do%20uprawnien/AllItems.aspx')
    // console.log(lista.getItems(camlQuery)
  }, console.log);
}).catch(console.log);

