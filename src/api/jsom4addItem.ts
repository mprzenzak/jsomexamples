import { JsomNode } from 'sp-jsom-node';
import { authConfig } from '../config';

(new JsomNode(authConfig)).wizard().then((settings) => {

  var ctx = SP.ClientContext.get_current();
  var customList = ctx.get_web().get_lists().getByTitle('Lista z JSOMa2');
  var itemCreateInfo = new SP.ListItemCreationInformation();
  var listItem = customList.addItem(itemCreateInfo);
  listItem.set_item('Title', 'test2');
  listItem.update();
  ctx.load(listItem);
  ctx.executeQueryAsync(
    function () { console.log('dodalem sobie taki item: ' + listItem.get_id()); },
  );
}).catch(console.log);